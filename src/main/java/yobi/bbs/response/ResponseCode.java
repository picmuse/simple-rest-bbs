package yobi.bbs.response;

/**
 * 응답코드
 * @author Dongseop
 */
public enum ResponseCode {
	SUCCESS(0),
	FAIL(1);
	
	private int code;

	private ResponseCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}
