package yobi.bbs.response;


/**
 * 응답객체 팩토리 메서드 제공
 * @author Dongseop
 */
public class ResponseBuilder {
	/**
	 * 성공 응답 시 별도의 컨텐츠가 없으면 이 클래스를 컨텐츠로 사용 
	 */
	public static class Success {
	}
	
	/**
	 * 실패 응답 시 별도의 컨텐츠가 없으면 이 클래스를 컨텐츠로 사용
	 */
	public static class Fail {
	}
	
	/**
	 * 컨텐츠 없는 성공 응답 반환
	 * @return
	 */
	public static Response<Success> success() {
		Response<Success> response = new Response<Success>();
		response.setCode(ResponseCode.SUCCESS.getCode());
		response.setMessage(ResponseCode.SUCCESS.name());
		return response;
	}
	
	/**
	 * 컨텐츠 가진 성공 응답 반환
	 * @param content
	 * @return
	 */
	public static <T> Response<T> success(T content) {
		Response<T> response = new Response<T>();
		response.setCode(ResponseCode.SUCCESS.getCode());
		response.setMessage(ResponseCode.SUCCESS.name());
		response.setContent(content);
		return response;
	}
	
	/**
	 * 컨텐츠 없는 실패 응답 반환
	 * @param responseCode
	 * @param message
	 * @return
	 */
	public static Response<Fail> fail(String error) {
		Response<Fail> response = new Response<Fail>();
		response.setCode(ResponseCode.FAIL.getCode());
		response.setMessage(ResponseCode.FAIL.name());
		response.setError(error);
		return response; 
	}
}
