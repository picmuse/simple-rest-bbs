package yobi.bbs.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 응답에 공통으로 포함되는 필드와 컨텐츠를 래핑한다
 * @author Dongseop
 * @param <T> 응답 컨텐츠 타입
 */
@JsonInclude(Include.NON_NULL)
public class Response<T> {
	private int code;
	private String message;
	private String error;
	private T content;
	
	/**
	 * @return 응답코드
	 * @see {@link ResponseCode}
	 */
	public int getCode() {
		return code;
	}
	
	/**
	 * @param code 응답코드
	 * @see {@link ResponseCode}
	 */
	public void setCode(int code) {
		this.code = code;
	}
	
	/**
	 * 응답코드에 해당하는 메시지를 반환
	 * @return 응답메시지
	 * @see {@link ResponseCode}
	 */
	public String getMessage() {
		return message;
	}
	
	/**
	 * 응답코드에 해당하는 메시지를 설정
	 * @param message 응답메시지
	 * @see {@link ResponseCode}
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return 응답내용
	 */
	public T getContent() {
		return content;
	}
	/**
	 * @param content 응답내용
	 */
	public void setContent(T content) {
		this.content = content;
	}
	
	/**
	 * 실패응답시 함께 반환할 에러내용을 반환
	 * @return 에러내용
	 */
	public String getError() {
		return error;
	}

	/**
	 * 실패응답시 함께 반환할 에러내용을 설정
	 * @param error 에러내용
	 */
	public void setError(String error) {
		this.error = error;
	}
}
