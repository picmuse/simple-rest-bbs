package yobi.bbs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 게시글
 * @author Dongseop
 */
@Entity
@JsonInclude(Include.NON_NULL)
public class Post {
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	
	@Column
	@NotNull @Size(min=1)
	private String author;
	
	@Column
	@NotNull @Size(min=1)
	private String title;
	
	@Column
	@NotNull @Size(min=1)
	private String content;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
