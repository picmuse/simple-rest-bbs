package yobi.bbs.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import yobi.bbs.entity.Post;
import yobi.bbs.exception.NoSuchPostException;
import yobi.bbs.repository.PostRepository;

/**
 * 게시글 저장/조회 메서드를 제공
 * @author Dongseop
 */
@Service
public class PostService {
	@Autowired
	private PostRepository postRepository;
	
	public void create(Post post) {
		Assert.notNull(post, "post은 null일 수 없습니다.");
		postRepository.save(post);
	}
	
	/**
	 * 게시글 수정
	 * @param post id필드값은 수정 대상 게시글의 id이다. 나머지 not null 필드값을 해당 게시글에 덮어쓴다. 
	 */
	public void modify(Post post) throws NoSuchPostException {
		Assert.notNull(post, "post은 null일 수 없습니다.");
		Assert.notNull(post.getId(), "post id는 null일 수 없습니다.");
		
		Post saved = get(post.getId());
		if (saved == null) {
			throw new NoSuchPostException(post.getId() + "에 해당하는 게시글을 찾을 수 없습니다.");
		}
		
		if (StringUtils.isNotEmpty(post.getAuthor())) {
			saved.setAuthor(post.getAuthor());
		}
		if (StringUtils.isNotEmpty(post.getTitle())) {
			saved.setTitle(post.getTitle());
		}
		if (StringUtils.isNotEmpty(post.getContent())) {
			saved.setContent(post.getContent());
		}

		postRepository.save(saved);
	}

	/**
	 * @param id
	 * @return 아이디에 해당하는 게시글이 없으면 null 반환
	 */
	public Post get(long id) {
		return postRepository.findOne(id);
	}
	
	/**
	 * 저장된 모든 게시글을 반환
	 * @return 게시글 리스트. 게시글이 없으면 빈 리스트를 반환
	 */
	public List<Post> list() {
		List<Post> postList = new ArrayList<Post>();
		
		Iterable<Post> iterable = postRepository.findAll();
		
		if (iterable != null) {
			Iterator<Post> iterator = iterable.iterator();
			
			if (iterator != null) {
				while (iterator.hasNext()) {
					postList.add(iterator.next());
				}
			}
		}

		return postList;
	}

	public void delete(Long id) throws NoSuchPostException {
		try {
			postRepository.delete(id);
		} catch (EmptyResultDataAccessException e) {
			throw new NoSuchPostException(id + "에 해당하는 게시글을 찾을 수 없습니다.", e);
		}
	}

}
