package yobi.bbs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ComponentScan({
	"yobi.bbs.controller",
	"yobi.bbs.service"})
@EnableJpaRepositories("yobi.bbs.repository")
@EntityScan("yobi.bbs.entity")
public class YobiBbsApplication extends WebMvcConfigurerAdapter {
	

    public static void main(String[] args) {
        SpringApplication.run(YobiBbsApplication.class, args);
    }
}
