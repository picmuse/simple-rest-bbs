package yobi.bbs.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import yobi.bbs.entity.Post;
import yobi.bbs.exception.NoSuchPostException;
import yobi.bbs.response.Response;
import yobi.bbs.response.ResponseBuilder;
import yobi.bbs.response.ResponseBuilder.Fail;
import yobi.bbs.response.ResponseBuilder.Success;
import yobi.bbs.service.PostService;

/**
 * 게시글 저장/조회 요청 핸들러
 * @author 임동섭
 */
@RestController
@RequestMapping("/posts")
public class PostController {
	private static final Logger logger = LoggerFactory.getLogger(PostController.class);
	@Autowired
	private PostService postService;
	
	@RequestMapping(method=RequestMethod.POST)
	public Response<Success> post(
			@RequestBody @Valid Post post) {
		postService.create(post);
		return ResponseBuilder.success(); 
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Response<List<Post>> list() {
		List<Post> posts = postService.list();
		posts.forEach((post) -> {post.setContent(null);});
		return ResponseBuilder.success(posts);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.POST)
	public Response<Success> modify(
			@PathVariable Long id, 
			@RequestBody Post post) throws NoSuchPostException {
		post.setId(id);
		postService.modify(post);
		return ResponseBuilder.success(); 
	}
	
	@RequestMapping(value="/{id}", params="action=delete")
	public Response<Success> delete(@PathVariable Long id) 
			throws NoSuchPostException {
		postService.delete(id);
		return ResponseBuilder.success(); 
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public Response<Post> get(@PathVariable Long id) 
					throws NoSuchPostException {
		Post article = postService.get(id);
		if (article == null) {
			throw new NoSuchPostException(id + "에 해당하는 게시글을 찾을 수 없습니다.");
		}
		return ResponseBuilder.success(article);
	}
	
	@ExceptionHandler(NoSuchPostException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Response<Fail> handleNoSuchArticleException(NoSuchPostException e) {
		return ResponseBuilder.fail(e.getMessage());
	}
	
	/**
	 * request body validation 실패시 호출
	 * @param e
	 * @return 바인딩 에러 중 첫번째 에러의 메시지
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Response<Fail> handleBadRequestException(MethodArgumentNotValidException e) {
		BindingResult bindingResult = e.getBindingResult();
		String message = "";
		
		if (bindingResult.hasFieldErrors()) {
			FieldError fieldError = bindingResult.getFieldError();
			message = String.format("[%s=%s] : %s", 
					fieldError.getField(), 
					fieldError.getRejectedValue(),
					fieldError.getDefaultMessage());
		} else {
			message = bindingResult.getGlobalError().getDefaultMessage();
		}
							;
		return ResponseBuilder.fail(message);
	}
	
	@ExceptionHandler(RuntimeException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Response<Fail> handleRuntimeException(RuntimeException e) {
		logger.error(ExceptionUtils.getFullStackTrace(e));
		return ResponseBuilder.fail("시스템 오류가 발생하였습니다.");
	}
}
