package yobi.bbs.exception;

/**
 * 게시글을 찾을 수 없음
 * @author Dongseop
 */
@SuppressWarnings("serial")
public class NoSuchPostException extends Exception {
	public NoSuchPostException(String message) {
		super(message);
	}

	public NoSuchPostException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
