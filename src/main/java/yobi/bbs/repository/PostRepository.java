package yobi.bbs.repository;

import org.springframework.data.repository.CrudRepository;

import yobi.bbs.entity.Post;

/**
 * 게시글의 저장/조회 메서드 제공
 * @author Dongseop
 */
public interface PostRepository extends CrudRepository<Post, Long> {

}
