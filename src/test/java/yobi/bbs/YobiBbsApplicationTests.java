package yobi.bbs;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import yobi.bbs.response.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = YobiBbsApplication.class)
@WebIntegrationTest("server.port:9000")
public class YobiBbsApplicationTests {
	private RestTemplate restTemplate = new TestRestTemplate();
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testList() {
		Response response = restTemplate.getForObject("http://localhost:9000/posts", Response.class);
		Assert.assertThat(response.getMessage(), Is.is("SUCCESS"));
	}
}
