요구사항
--------------
* JDK 1.8+
* Maven 3+

설치
--------------

```bash
unzip -d yobi-bbs yobi-bbs.zip
cd yobi-bbs
mvn package
```

서버구동
---------------

```bash
java -jar target/yobi-bbs-0.0.1-SNAPSHOT.jar
```


테스트
----------------

명령

```bash
curl -X GET http://localhost:8080/posts
```

응답

```json
{
"code": 0,
"message": "SUCCESS",
"content": []
}
```
