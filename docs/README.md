이 문서는 API 사용방법을 기술한다.
모든 API의 요청과 응답 body의 content type은 application/json이다.
응답 body의 형식은 아래와 같다.

응답 body json 필드
------------------------------

* code : {Number} 응답코드 0이면 성공, 1이면 실패 [필수]
* message : {String} 응답코드에 해당하는 설명. SUCCESS 또는 FAIL [필수]
* error : {String} 실패시 원인 설명. 성공인 경우 필드 생략 [옵션]
* content : {Object} 응답 내용. 필드 생략 가능 [옵션]

응답 body 예시 - 성공
------------------------------

```json
{
"code": 0,
"message": "SUCCESS",
"content" : [
		{"foo": "boo"},
		{"foo": "boo"},
	]
}
```

응답 body 예시 - 성공(content 필드 생략)
------------------------------

```json
{
"code": 0,
"message": "SUCCESS"
}
```

응답 body 예시 - 실패
------------------------------

```json
{
"code": 1,
"message": "FAIL",
"error" : "foo does not exists"
}
```

이하 API 목록이다. 목록의 내용은 다음 규칙을 따른다.

* 목록의 제목은 각 API이며 "url HTTP_METHOD"로 표시한다. 
가령, url이 "/foo"이고 Http method가 "GET"인 API는 목록에 "/foo GET"으로 표시한다.
* url 변수가 있을 경우 "{varible name}"으로 표시한다.
가령, url이 "/foo/{boo}"인 경우 "{boo}"는 url 변수이다.
* "요청" 항목은 body의 json field를 기술한다. url 변수는 있을 경우만 표시한다.
* "응답" 항목은 성공 실패 조건과 성공 시 응답 body의 content 필드값만 기술한다. 
나머지 응답 body 필드는 위의 공통 형식을 따른다.
* 요청과 응답 body의 field 설명은 "name : {Type} Description [필수|옵션]" 형식으로 표시한다.
"[필수]"인 field는 생략할 수 없으며, "[옵션]"은 생략할 수 있다.

/posts GET
============================

모든 게시글을 반환한다

요청
----------------------

body

	* 없음

응답
----------------------

성공

	* 게시글 리스트를 조회함

실패

	* 게시글 리스트를 조회할 수 없음


body

	게시글 리스트. 게시글은 다음 필드를 포함한다.

	* id : {Number} 게시글 아이디. [필수]
	* author : {String} 저자. [필수]
	* title : {String} 제목. [필수]

예시
----------------------

요청

```bash
curl -X GET http://localhost:8080/posts
```

응답

```json
{
"code": 0,
"message": "SUCCESS",
"content": [
		{"id": 1, "author": "Pasternak", "title": "Zhivago"},
		{"id": 2, "author": "Fitzgerald", "title": "Greate Gatsby"}
	]
}
```

/posts POST
============================

게시글을 저장한다.

요청
----------------------

body

	* author: {String} 한글자 이상. [필수]
	* title: {String} 한글자 이상. [필수]
	* content: {String} 한글자 이상. [필수]

응답
----------------------

성공

	* 게시글이 저장됨

실패

	* 게시글을 저장할 수 없음

body

	* content 필드 없음

예시
----------------------

요청

```bash
curl -H "Content-Type:application/json" -X POST http://localhost:8080/posts -d \
{
"author": "Pasternak",
"title": "Zhivago",
"content": "So humble"
}
```

응답

```json
{
"code": 0,
"message": "SUCCESS"
}
```


/posts/{id} GET
============================

지정한 게시글을 반환한다.

요청
----------------------

url 변수

* id : {Number} 반환할 게시글의 아이디

body

* 없음
	
응답
----------------------

성공

	* 게시글을 반환함

실패

	* 게시글을 반환할 수 없음

body

	게시글

	* id : {Number} 게시글 아이디. [필수]
	* author : {String} 저자. [필수]
	* title : {String} 제목. [필수]
	* content : {String} 내용. [필수]

예시
----------------------

요청

```bash
curl -X GET http://localhost:8080/posts/10
```

응답

```json
{
"code": 0,
"message": "SUCCESS",
"content" : {
		"id": 10
		"author": "Pasternak",
		"title": "Zhivago",
		"content": "Look! A humble programmer here"
	}
}
```

/posts/{id} POST
============================

지정한 게시글을 수정한다.

요청
----------------------

url 변수

	* id : {Number} 반환할 게시글의 아이디

body

	* author: {String} 한글자 이상. [옵션]
	* title: {String} 한글자 이상. [옵션]
	* content: {String} 한글자 이상. [옵션]
	
응답
----------------------

성공

	* 게시글이 수정됨

실패

	* 게시글을 수정할 수 없음

body

	* content 필드 없음	

예시
----------------------

요청

```bash
curl -H "Content-Type:application/json" -X POST http://localhost:8080/posts/10 -d \
{
	"author": "foo"
}
```

응답

```json
{
"code": 0,
"message": "SUCCESS"
}
```

/posts/{id}?action=delete GET
============================

지정한 게시글을 삭제한다.

요청
----------------------

url 변수

	* id : {Number} 삭제할 게시글의 아이디

body

	* 없음
	
응답
----------------------

body
	* content 필드없음

예시
----------------------

요청

```bash
curl -X GET http://localhost:8080/posts/10?action=delete
```

응답

```json
{
"code": 0,
"message": "SUCCESS"
}
```

